# Client-Server

Client/Server application to load big (&gt;100mb) files with data protection.
- Client sends list of files from his folder.
- Server requests file for loading via port 8400.
- Client sends size of file and start transferring.
- When file transferring for 10% server request to change transferring port to
(+5) 8405.
- The rest of file has to be transferred via new port
- When file transferred for 20%  server request to change transferred port to
(+5) 8410.
- The rest of file has to be transferred via new port..... till 100%

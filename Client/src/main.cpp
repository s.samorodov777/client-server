#include <iostream>

#include <boost/asio/io_service.hpp>

#include "client.h"
#include "../include/logger.h"

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        std::cerr << "Usage: client <address> <port> <filePath>\n";
        char arg1[] = "localhost";
        char arg2[] = "8400";
        char arg3[] = "Donne.Moi.des.Ailes.2019.BDRip.720p.seleZen.mkv";
        argv[1] = arg1;
        argv[2] = arg2;
        argv[3] = arg3;
    }

    //Logger::instance().setOptions("client_%3N.log", 1 * 1024 * 1024, 10 * 1024 * 1024);

    auto address = argv[1];
    auto port = argv[2];
    auto filePath = argv[3];

    try
    {
        boost::asio::io_service ioService;

        boost::asio::ip::tcp::resolver resolver(ioService);
        auto endpointIterator = resolver.resolve({address, port});
        Client client(ioService, endpointIterator, filePath);

        ioService.run();
    }
    catch (std::fstream::failure &e)
    {
        std::cerr << e.what() << "\n";
    }
    catch (std::exception &e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
